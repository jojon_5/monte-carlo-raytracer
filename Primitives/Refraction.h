//
//  glass.h
//  MonteCarlo
//
//  Created by Johannes Deligiannis on 24/11/13.
//  Copyright (c) 2013 Johannes Deligiannis. All rights reserved.
//

#ifndef MonteCarlo_Refraction_h
#define MonteCarlo_Refraction_h
#include "Util/Ray.h"


class Refraction {
public:
    virtual float refracitveIndex(float wavelength) const = 0;
};

#endif
